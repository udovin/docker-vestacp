FROM debian:8

MAINTAINER Ivan Udovin <wilcot@ya.ru>

RUN \
	export TERM=linux \
	&& apt-get -y update \
	&& apt-get -y install \
		curl \
	&& curl -O http://vestacp.com/pub/vst-install-debian.sh \
	&& bash vst-install-debian.sh \
		--interactive no \
		--password admin \
		--nginx yes \
		--phpfpm yes \
		--apache no \
		--named yes \
		--remi no \
		--vsftpd no \
		--proftpd no \
		--iptables yes \
		--fail2ban yes \
		--quota no \
		--exim yes \
		--dovecot yes \
		--spamassassin no \
		--clamav no \
		--mysql yes \
		--postgresql no \
	&& rm -f vst-install-debian.sh \
	&& apt-get -y clean

RUN \
	cd /usr/local/vesta/data/ips \
	&& mv -f 172.*.*.* 127.0.0.1 \
	&& cd / \
	&& mkdir /vesta \
	&& mkdir /vesta/var \
	&& mkdir /vesta/var/lib \
	&& mv -f /root /vesta/root \
	&& rm -rf /root \
	&& ln -s /vesta/root /root \
	&& mv -f /var/lib/mysql /vesta/var/lib/mysql \
	&& rm -rf /var/lib/mysql \
	&& ln -s /vesta/var/lib/mysql /var/lib/mysql

COPY vestacp.sh /vestacp.sh

RUN chmod 0744 /vestacp.sh

ENTRYPOINT ["/vestacp.sh"]

VOLUME ["/vesta", "/home"]

EXPOSE 80 443 8083 22 53 3306
