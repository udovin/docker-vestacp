#!/bin/bash

export VESTA=/usr/local/vesta

cd /usr/local/vesta/bin \
	&& bash v-start-service vesta \
	&& bash v-start-service nginx \
	&& bash v-start-service bind9 \
	&& bash v-start-service php5-fpm \
	&& bash v-start-service mysql \
	&& bash v-start-service exim4 \
	&& bash v-start-service dovecot \
	&& bash v-start-service cron

# Container should be running
sleep infinity & wait
